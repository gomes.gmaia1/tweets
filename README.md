## Lista de Arquivos no repositório

* **train.csv** Base de dados utilizada para a análise;
* **pratica.R** Código utilizado para a preparação dos dados e modelagem;
* **pratica.pdf** Relatório de análise do problema.
* **teorica.Rmd** Arquivo de documentação da parte teórica em formato r markdown;
* **teorica.pdf** Relatório de da parte teórica.
